This is the prototype for Aidan Stawasz's camp app program.

This prototype demonstrates how a user could interact with a camp schedule via a GUI.
The data in Camps_Matrix.csv is collected from three weeks of camps run at an FCPA rec center in 2023.


Functionality demonstrated in prototype:
- Read in camps data from csv file
- Create a GUI to display camp schedules
- User may choose which week of camp to view
- User may drag and resize time blocks for different camp activities


The full version of this software will be adapted in Java and include much more functionality beyond this proof of concept.
Future functionality (In-Progress):
- Home screen for navigating between different app functions
- Attendance tracker
- Digital camp roster
- Room schedule manager (user may add/remove camps and activities from schedule, which will be saved locally)